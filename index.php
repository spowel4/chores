<?php 

    require 'includes/functions.php';

    $conn = connect($config);
    if ($conn) {
        $persons = return_all_records('persons', $conn);
        $persons_length = sizeof($persons);
        $chores = return_all_records('chore', $conn);
        $chores_length = sizeof($chores);
        $frequencies = return_all_records('frequencies', $conn);
        var_dump($persons);
        var_dump($persons_length);
        var_dump($chores);
        var_dump($chores_length);
        var_dump($frequencies);
    } else {
        echo "Could not connect to the database";
    }
 ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head lang="en">
        <!-- index.php -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Chores Assignment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <h1>Chores Assignment</h1>
        <form id="choreChartForm" name="choreChartForm" action="saveChanges.php" method="post">
            <!-- <div id="datepicker"></div> -->
            <input type="hidden" name="choresLength" value='<?= $chores_length ?>'>
            <input type="hidden" name="personsLength" value='<?= $persons_length ?>'>
            <p>
                <span><label for="datepickerFrom">From Date: </label><input type="text" id="datepickerFrom" name="datepickerFrom" /></span>
                <span><label for="datepickerTo">To Date: </label><input type="text" id="datepickerTo" name="datepickerTo" /></span>
            </p>
            <!-- </div> -->
            <script>
                $("#datepickerFrom").datepicker();
                $("#datepickerTo").datepicker();
            </script>
            <br>
            <table id="choresChart">
                <tr>
                    <?php for ($i = 0; $i < $persons_length; $i++) : ?>
                        <th colspan="2">
                            <select name="<?= $person . $i ?>" id="<?= $person . $i ?>">
                                <?php foreach ($persons as $person) : ?>
                                    <option value="<?= $person['name']; ?>">
                                        <?= $person['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </th>
                    <?php endfor; ?>
                </tr>
                <?php for ($i = 0; $i < $chores_length; $i++) : ?>
                    <tr>
                        <?php for ($j = 0; $j < $persons_length; $j++) : ?>
                            <td>
                                <select name='<?= chore . $i . _person . $j?>'>
                                    <option value="blank"></option>
                                    <?php foreach ($chores as $chore) : ?>
                                        <option value="<?= $chore['choreName']; ?>">
                                            <?= $chore['choreName']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <select name='<?= frequency . $i . _person . $j?>'>
                                    <option value="blank"></option>
                                    <?php foreach ($frequencies as $frequency) : ?>
                                        <option value='<?= $frequency['frequency']; ?>'>
                                            <?= $frequency['frequency']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        <?php endfor; ?>
                    </tr>
                <?php endfor; ?>
            </table>
            <input type="submit" name="saveChoresAssignments" value="Save Changes">
            <input type="button" name="printChoresAssignments" value="Print Chart">
        </form>
        <ul>
            <li><a href="choresChanges.php">Add or Edit Chore</a></li>
            <li><a href="editFrequencies.php">Add or Edit Frequency</a></li>
        </ul>

        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
        <!-- <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script> -->
        <!-- <script>window.jQuery || document.write('<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"><\/script> -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
