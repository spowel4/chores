<?php
    require 'config.php';


/*
|--------------------------------------------------------
|             Database Connection function               |
|--------------------------------------------------------   
*/
function connect($config) {
    try {
        $conn = new PDO('mysql:host=localhost;dbname=chores',
                        $config['DB_USERNAME'],
                        $config['DB_PASSWORD']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
        return false;
    }
}

/*
|--------------------------------------------------------
|             Create Record function                     |
|--------------------------------------------------------
*/
function create_chore_record($table_name, $value, $conn) {
    try {
        $sqlText = "INSERT INTO $table_name (choreName) VALUES(:value)";
        //echo "sqlText: " . $sqlText . "<br>";
        $stmt = $conn->prepare($sqlText);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
     
      # Affected Rows?
      //echo $stmt->rowCount();
      if ($stmt->rowCount() > 0) {
        return true;
      } else {
        return false;
      }
    } catch(PDOException $e) {
      echo 'Error: ' . $e->getMessage();
      return false;
    }
}

function create_frequency_record($table_name, $value, $conn) {
    try {
        $sqlText = "INSERT INTO $table_name (frequency) VALUES(:value)";
        //echo "sqlText: " . $sqlText . "<br>";
        $stmt = $conn->prepare($sqlText);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
     
      # Affected Rows?
      //echo $stmt->rowCount();
      if ($stmt->rowCount() > 0) {
        return true;
      } else {
        return false;
      }
    } catch(PDOException $e) {
      echo 'Error: ' . $e->getMessage();
      return false;
    }
}


/*
|--------------------------------------------------------
|             Return Record function                     |
|--------------------------------------------------------
*/
function return_record($table_name, $id) {
    $sqlText = 'SELECT * FROM $table_name WHERE id = :id';
    $stmt = $conn->prepare($sqlText);
    $stmt->setFetchMode(PDO::FETCH_OBJ);    // sets all queries to object mode
    #$stmt->setFetchMode(PDO::FETCH_ASSOC); // sets all queries to associative array mode
    $stmt->execute(array('id' => $id));
    while ($row = $stmt->fetch()) {
        print_r($row);
    }

    // alternate method
    #$stmt->bindParam(':id', $id, PDO::PARAM_INT);  // string would be PARAM_STR
    #$stmt->execute();

    #while ($row = $stmt->fetch()) {
    #   print_r($row);
    #}

    // an alternate while loop, returns results as objects; the default method returns an array
    #while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
    #   print_r($row);
    #}
}




/*
|--------------------------------------------------------
|             Return All Records function                |
|--------------------------------------------------------
*/
function return_all_records($table_name, $conn) {
    try {
        $stmt = $conn->query("SELECT * FROM $table_name");
        $result = $stmt->fetchAll();
        if (count($result)) {
            return $result;
        } else {
            echo "No records returned";
            return false;
        }
        //return ($result->rowCount() > 0) ? $result : false;
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        return false;
    }
}





/*
|---------------------------------------------------------
|             Update Record function                      |
|---------------------------------------------------------
*/
function update_record($table_name, $value, $id, $conn) {
    try {
        echo "table name: $table_name; value: $value; id: $id<br>";
        $result = $conn->prepare("INSERT INTO $table_name VALUES(:value)");
        $result->bindParam(':value', $value);
        $result->execute();
        echo "row count: " . $result->rowCount();
        return ($result->rowCount() > 0) ? $result : false;
    } catch(PDOException $e) {
      echo 'Error: ' . $e->getMessage();
      return false;
    }
}



/*
|----------------------------------------------------------
|             Delete Record function                       |
|----------------------------------------------------------
*/
function delete_record($table_name, $id, $conn) {
    try {
        $result = $conn->prepare("DELETE FROM $table_name WHERE id = :id");
        $result->bindParam(':id', $id);
        $result->execute();
        return ($result->rowCount() > 0) ? $result : false;
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        return false;
    }
}