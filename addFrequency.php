<?php

  require 'includes/functions.php';

    $conn = connect($config);
    if (!$conn) {
      echo "Could not connect to the database";
    } else {
      $table_name = $_POST['table_name'];
      $frequency_name = $_POST['frequency_name'];
      echo '$table_name: ' . $table_name . '<br>$frequency_name: ' . $frequency_name . '<br>';
      $result = create_frequency_record($table_name, $frequency_name, $conn);
    }
?>

<!doctype html>

<html lang="en">
<head>
  <!-- addFrequency.php -->
  <meta charset="utf-8">
  <title>Frequencies Changes</title>
</head>
<body>
  <?php
    if ($result) {
      echo "Frequency successfully added<br>";
    }
  ?>
  <a href="editFrequencies.php">Add/Edit Another Frequency</a><br>
  <a href="index.php">Return to Chores Assignment Chart</a>
</body>
</html>
