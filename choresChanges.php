<?php 

    require 'includes/functions.php';

    $conn = connect($config);
    if ($conn) {
        $chores = return_all_records('chore', $conn);
    } else {
        echo "Could not connect to the database";
    }
 ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head lang="en">
        <!-- choresChanges.php -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Chores Assignment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <h1>Edit Chores</h1>

        <form id="choresEdit" action="deleteChores.php" method="post">
          <table id="choresTable" class="altRows">
            <tr>
              <th colspan="2">ID</th>
              <th colspan="2">Chore</th>
              <th colspan="4">Delete?</th>
            </tr>
            <?php foreach ($chores as $chore) : ?>
              <tr>
                <td colspan="2">
                  <input type="text" value="<?= $chore['id']; ?>" size="1" name="id[]" />
                  <!-- <label name="id[]"><?=$chore['id']; ?></label> -->
                </td>
                <td colspan="2">
                  <input type="text" value="<?= $chore['choreName']; ?>" name="choreName[]" />
                </td>
                <td colspan="2">
                  <input type="checkbox" name="delete[]" value="<?= $chore['id']; ?>" />
                </td>
              </tr>
            <?php endforeach; ?>
          </table>
          <input type="submit" value="Submit Changes" />
        </form>
        <hr>
        <h1>Add Chore</h1>

        <form id="choresAdd" action="addChore.php" method="post">
          <p>
            <label for="chore_name">Chore Description</label>
            <input type="text" id="chore_name" name="chore_name" />
            <input type="hidden" value="chore" name="table_name";
          </p>
          <p><input type="submit" value="Add New Chore" /></p>
        </form>
        <a href="index.php">Return to Chores Assignment Chart</a>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
