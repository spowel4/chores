<?php

  require 'includes/functions.php';

    $conn = connect($config);
    if (!$conn) {
      echo "Could not connect to the database";
    }
    //print_r($_POST);
    //die();
?>

<!doctype html>

<html lang="en">
<head>
  <!-- saveChanges.php -->
  <meta charset="utf-8">
  <title>Save Changes</title>
</head>
<body>
  <?php
    $table_name="choreAssignments";
    $result = update_record($table_name, $value, $id, $conn);
    if ($result) {
      echo "<h3>Successfully saved changes</h3>";
    } else {
      echo "<h3>No records updated</h3";
    }
  ?>
  <a href="index.php">Return to Chores Chart</a>
</body>
</html>
