<?php

  require 'includes/functions.php';

    $conn = connect($config);
    if (!$conn) {
      echo "Could not connect to the database";
    }
?>

<!doctype html>

<html lang="en">
<head>
  <!-- editChores.php -->
  <meta charset="utf-8">
  <title>Chores Changes</title>
</head>
<body>
  <?php
    if ($result) {
      echo "<h3>Successfully updated and/or deleted records</h3>";
    }
  ?>
  <a href="index.php">Return to Chores Chart</a>
</body>
</html>
