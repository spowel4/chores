<?php

  require 'includes/functions.php';

    $conn = connect($config);
    if (!$conn) {
      echo "Could not connect to the database";
    } else {
      $table_name = $_POST['table_name'];
      $chore_name = $_POST['chore_name'];
      //echo '$table_name: ' . $table_name . '<br>$chore_name: ' . $chore_name . '<br>';
      $result = create_chore_record($table_name, $chore_name, $conn);
    }
?>

<!doctype html>

<html lang="en">
<head>
  <!-- addChore.php -->
  <meta charset="utf-8">
  <title>Chores Changes</title>
</head>
<body>
  <?php
    if ($result) {
      echo "Chore successfully added<br>";
    }
  ?>
  <a href="choresChanges.php">Add/Edit another chore</a><br>
  <a href="index.php">Return to Chores Assignment Chart</a>
</body>
</html>
