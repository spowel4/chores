<?php

  require 'includes/functions.php';

    $conn = connect($config);
    if (!$conn) {
      echo "Could not connect to the database";
    }
    echo "<pre>";
    var_dump($_POST);
    echo "</pre>";
    //die();
?>

<!doctype html>

<html lang="en">
<head>
  <!-- saveFrequencyChanges.php -->
  <meta charset="utf-8">
  <title>Save Frequency Changes</title>
</head>
<body>
  <?php
    $table_name="frequencies";
    #$value = 7;
    #$id = 7;
    $result = update_record($table_name, $value, $id, $conn);
    if ($result) {
      echo "<h3>Successfully saved changes</h3>";
    } else {
      echo "<h3>No records updated</h3";
    }
  ?>
  <a href="index.php">Return to Chores Chart</a>
</body>
</html>
