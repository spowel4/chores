<?php

  require 'includes/functions.php';

    $conn = connect($config);
    if (!$conn) {
      echo "Could not connect to the database";
    }

  $delete_array = $_POST['delete'];
  $id_array = $_POST['id'];
  $chore_array = $_POST['choreName'];

  if (sizeof($delete_array) > 0) {
    for ($i = 0; $i < sizeof($id_array); $i++) {
      for ($j = 0; $j < sizeof($delete_array); $j++) {
        if ($id_array[$i] == $delete_array[$j]) {
          $result = delete_record('chore', $id_array[$i], $conn);
        } else {
        }
      }
    }
  }
?>

<!doctype html>

<html lang="en">
<head>
  <!-- deleteChores.php -->
  <meta charset="utf-8">
  <title>Chores Changes</title>
</head>
<body>
  <?php
    if ($result) {
      echo "<h3>Successfully updated and/or deleted records</h3>";
    }
  ?>
  <a href="choresChanges.php">Add/Edit Another Chore</a>
  <a href="index.php">Return to Chores Assignment Chart</a>
</body>
</html>
